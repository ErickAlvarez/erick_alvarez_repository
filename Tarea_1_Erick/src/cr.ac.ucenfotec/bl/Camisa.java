package cr.ac.ucenfotec.bl;

public class Camisa {
    private int idCamisa;
    private String colorCamisa;
    private String tamanioCamisa;
    private String descripcion;
    private double precio;

    public Camisa(int idCamisa, String colorCamisa, String tamanioCamisa, String descripcion, double precio) {
        setIdCamisa(idCamisa);
        setColorCamisa(colorCamisa);
        setDescripcion(descripcion);
        setTamanioCamisa(tamanioCamisa);
        setPrecio(precio);
    }

    public int getIdCamisa() {
        return idCamisa;
    }

    public void setIdCamisa(int idCamisa) {
        this.idCamisa = idCamisa;
    }

    public String getColorCamisa() {
        return colorCamisa;
    }

    public void setColorCamisa(String colorCamisa) {
        this.colorCamisa = colorCamisa;
    }

    public String getTamanioCamisa() {
        return tamanioCamisa;
    }

    public void setTamanioCamisa(String tamanioCamisa) {
        this.tamanioCamisa = tamanioCamisa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "idCamisa:  " + idCamisa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Camisa camisa = (Camisa) o;
        return getIdCamisa() == camisa.getIdCamisa();
    }
}
