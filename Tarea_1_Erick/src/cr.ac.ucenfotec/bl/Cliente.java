package cr.ac.ucenfotec.bl;

import java.util.Objects;

public class Cliente {
    private String nombre;
    private String apellido;
    private String segundoApellido;
    private String direccionExacta;
    private String correoElectronico;

    public Cliente(String nombre, String apellido, String segundoApellido, String direccionExacta, String correoElectronico) {
       setNombre(nombre);
       setApellido(apellido);
       setSegundoApellido(segundoApellido);
       setDireccionExacta(direccionExacta);
       setCorreoElectronico(correoElectronico);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccionExacta() {
        return direccionExacta;
    }

    public void setDireccionExacta(String direccionExacta) {
        this.direccionExacta = direccionExacta;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Override
    public String toString() {
        return
                "Correo Electronico: " + correoElectronico+"\n"+ " Nombre:" + nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(getCorreoElectronico(), cliente.getCorreoElectronico());
    }
}
