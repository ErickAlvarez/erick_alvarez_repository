package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Camisa;
import cr.ac.ucenfotec.bl.Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Camisa> camisas = new ArrayList<>();
    static ArrayList<Cliente> clientes = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        int opcion = 0;
        do {
            opcion = pintarMenu();
            switch (opcion) {
                case 1:
                    agregarCamisa();
                    break;
                case 2:
                    verCamisas();
                    break;

                case 3:
                    agregarCliente();
                    break;
                case 4:
                    verCliente();
                    break;

            }
        } while (opcion != 0);
    }

    public static int pintarMenu() throws IOException {
        out.println("--- MENU ---");
        out.println("1. Agregar Camisas");
        out.println("2. Ver Camisas");
        out.println("3. Agregar Cliente");
        out.println("4. Ver Cliente");
        out.println("5.Salir");

        int opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    public static void agregarCamisa() throws IOException {
        out.print("Digite el id de la camisa: ");
        int idCamisa = Integer.parseInt(in.readLine());
        out.println("Digite el color de la camisa");
        String color = in.readLine();
        out.println("Digite la descripcion de la camisa");
        String descripcion = in.readLine();
        out.println("Digite el tamaño de la camisa S/L/M");
        String tamanio = in.readLine();
        out.println("Digite el precio de la camisa: ");
        double precio = Double.parseDouble(in.readLine());
        Camisa camisa = new Camisa(idCamisa, color, descripcion, tamanio, precio);
        camisas.add(camisa);
        out.println("Camisa registrada con exito ! ");
    }

    public static void verCamisas() {
        for (int i = 0; i < camisas.size(); i++) {
            Camisa chema = camisas.get(i);
            out.println((i + 1) + ")" + chema.toString());
            out.println("-----");
        }
    }

    public static void agregarCliente() throws IOException {
        out.print("Digite el nombre: ");
        String nombre = in.readLine();
        out.print("Digite el primer apellido: ");
        String primerApellido = in.readLine();
        out.print("Digite el segundo apellido: ");
        String segundoApellido = in.readLine();
        out.print("Digite la direccion: ");
        String direccion = in.readLine();
        out.print("Digite el correo: ");
        String correo = in.readLine();
        Cliente cliente = new Cliente(nombre, primerApellido, segundoApellido, direccion, correo);
        boolean existe = false;
        for (int i = 0; i < clientes.size(); i++) {
            if (clientes.get(i).equals(cliente)) {
                existe = true;
                out.println("El correo ya existe, cliente no registrado!");
            }
        }
        if (!existe) {
            clientes.add(cliente);
            out.println("Cliente registrado con exito ! ");
        }
    }

    public static void verCliente() {
        for (int i = 0; i < clientes.size(); i++) {
            Cliente cliente = clientes.get(i);
            out.println((i + 1) + ")" + cliente.toString());
            out.println("-----");
        }
    }

}
